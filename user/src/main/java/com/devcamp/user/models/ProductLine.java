package com.devcamp.user.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "product_lines")
public class ProductLine {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "product_line", unique = true)
	private String productLine;

	private String description;

	@OneToMany(mappedBy = "productLine", cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonIgnore
	private List<Product> products = new ArrayList<>();

	public ProductLine() {
	}

	public ProductLine(int id, String productLine, String description, List<Product> products) {
		this.id = id;
		this.productLine = productLine;
		this.description = description;
		this.products = products;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProductLine() {
		return productLine;
	}

	public void setProductLine(String productLine) {
		this.productLine = productLine;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

}
