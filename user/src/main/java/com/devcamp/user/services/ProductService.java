package com.devcamp.user.services;

import com.devcamp.user.models.Product;
import com.devcamp.user.repository.ProductRepository;
import com.devcamp.user.response.BaseResponse;
import com.devcamp.user.response.DatatablePage;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

@Service
public class ProductService {
    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public BaseResponse<Product> getAllProducts(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Product> productPage = productRepository.findAll(pageable);
        BaseResponse<Product> response = new BaseResponse<Product>();
        response.setTotalCount(productPage.getTotalElements());
        response.setData(productPage.getContent());
        return response;
    }

    public BaseResponse<Product> getProductsByProductLineId(int page, int size, int productLineId) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Product> productPage = productRepository.findByProductLineId(productLineId, pageable);
        BaseResponse<Product> response = new BaseResponse<Product>();
        response.setTotalCount(productPage.getTotalElements());
        response.setData(productPage.getContent());
        return response;
    }

    public BaseResponse<Product> getProductsByFilter(int page, int size, Integer productLineId, String productVendor,
            BigDecimal priceFrom, BigDecimal priceTo) {
        Pageable pageable = PageRequest.of(page, size);

        Page<Product> productPage = productRepository.filterProduct(productVendor, productLineId, priceFrom,
                priceTo, pageable);
        BaseResponse<Product> response = new BaseResponse<Product>();
        response.setTotalCount(productPage.getTotalElements());
        response.setData(productPage.getContent());
        return response;
    }

    public DatatablePage<Product> getAllProductDataTable(HttpServletRequest request) {
        // get the data
        int size = Integer.parseInt(request.getParameter("length"));
        int page = Integer.parseInt(request.getParameter("start")) / size;
        System.out.println(request.getParameter("order[0][column]"));
        System.out.println("columns[" + request.getParameter("order[0][column]") + "][data]");
        String sortColName = request.getParameter("columns[" + request.getParameter("order[0][column]") + "][data]");
        String order = request.getParameter("order[0][dir]");
        List<Product> products = new ArrayList<>();
        if (order.equals("desc")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).descending());
            productRepository.findAll(pageable).forEach(products::add);
        } else {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).ascending());
            productRepository.findAll(pageable).forEach(products::add);
        }

        long total = productRepository.count();
        DatatablePage<Product> data = new DatatablePage<Product>();
        data.setData(products);
        data.setRecordsFiltered((int) total);
        data.setRecordsTotal((int) total);
        data.setDraw(Integer.parseInt(request.getParameter("draw")));
        return data;
    }

    public Product findProductById(Integer id) {
        return productRepository.findById(id).orElse(null);

    }

    public Product createProduct(Product product) {
        return productRepository.save(product);
    }

    public Product updateProduct(Integer id, Product product) {
        Optional<Product> optionalProduct = productRepository.findById(id);
        if (optionalProduct.isPresent()) {
            Product existingProduct = optionalProduct.get();
            existingProduct.setProductCode(product.getProductCode());
            existingProduct.setProductName(product.getProductName());
            existingProduct.setProductDescription(product.getProductDescription());
            existingProduct.setProductLine(product.getProductLine());
            existingProduct.setProductScale(product.getProductScale());
            existingProduct.setProductVendor(product.getProductVendor());
            existingProduct.setQuantityInStock(product.getQuantityInStock());
            existingProduct.setBuyPrice(product.getBuyPrice());
            existingProduct.setOrderDetails(product.getOrderDetails());
            existingProduct.setProductPhotos(product.getProductPhotos());
            return productRepository.save(existingProduct);
        } else {
            return null;
        }
    }

    public void deleteProduct(Integer id) {
        productRepository.deleteById(id);
    }
}
