package com.devcamp.user.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import com.devcamp.user.models.OrderDetail;
import com.devcamp.user.repository.OrderDetailRepository;
import com.devcamp.user.response.DatatablePage;

@Service
public class OrderDetailService {
    private final OrderDetailRepository orderDetailRepository;

    public OrderDetailService(OrderDetailRepository orderDetailRepository) {
        this.orderDetailRepository = orderDetailRepository;
    }

    public List<OrderDetail> getAllOrderDetails(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<OrderDetail> orderDetailPage = orderDetailRepository.findAll(pageable);
        return orderDetailPage.getContent();
    }

    public DatatablePage<OrderDetail> getAllOrderDetailDataTable(HttpServletRequest request) {
        // get the data
        int size = Integer.parseInt(request.getParameter("length"));
        int page = Integer.parseInt(request.getParameter("start")) / size;
        System.out.println(request.getParameter("order[0][column]"));
        System.out.println("columns[" + request.getParameter("order[0][column]") + "][data]");
        String sortColName = request.getParameter("columns[" + request.getParameter("order[0][column]") + "][data]");
        String order = request.getParameter("order[0][dir]");
        List<OrderDetail> orderDetails = new ArrayList<>();
        if (order.equals("desc")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).descending());
            orderDetailRepository.findAll(pageable).forEach(orderDetail -> {
                try {
                    orderDetails.add(orderDetail);
                } catch (EntityNotFoundException e) {
                    // Handle the exception or simply continue to the next order detail
                    // For example, you can log the error and move on:
                    System.out.println("Product not found for OrderDetail with ID: " + orderDetail.getId());
                }
            });
        } else {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).ascending());
            orderDetailRepository.findAll(pageable).forEach(orderDetail -> {
                try {
                    orderDetails.add(orderDetail);
                } catch (EntityNotFoundException e) {
                    // Handle the exception or simply continue to the next order detail
                    // For example, you can log the error and move on:
                    System.out.println("Product not found for OrderDetail with ID: " + orderDetail.getId());
                }
            });
        }
        long total = orderDetailRepository.count();
        DatatablePage<OrderDetail> data = new DatatablePage<OrderDetail>();
        data.setData(orderDetails);
        data.setRecordsFiltered((int) total);
        data.setRecordsTotal((int) total);
        data.setDraw(Integer.parseInt(request.getParameter("draw")));
        return data;
    }

    public OrderDetail getOrderDetailById(int id) {
        Optional<OrderDetail> optionalOrderDetail = orderDetailRepository.findById(id);
        return optionalOrderDetail.orElse(null);
    }

    public OrderDetail createOrderDetail(OrderDetail orderDetail) {
        return orderDetailRepository.save(orderDetail);
    }

    public OrderDetail updateOrderDetail(int id, OrderDetail orderDetail) {
        Optional<OrderDetail> optionalOrderDetail = orderDetailRepository.findById(id);
        if (optionalOrderDetail.isPresent()) {
            OrderDetail existingOrderDetail = optionalOrderDetail.get();
            existingOrderDetail.setQuantityOrder(orderDetail.getQuantityOrder());
            existingOrderDetail.setPriceEach(orderDetail.getPriceEach());
            return orderDetailRepository.save(existingOrderDetail);
        } else {
            return null;
        }
    }

    public void deleteOrderDetail(int id) {
        orderDetailRepository.deleteById(id);
    }
}
