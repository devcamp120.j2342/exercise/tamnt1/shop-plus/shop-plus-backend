package com.devcamp.user.services;

import com.devcamp.user.models.Employee;
import com.devcamp.user.repository.EmployeeRepository;
import com.devcamp.user.response.DatatablePage;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getAllEmployees(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Employee> employeePage = employeeRepository.findAll(pageable);
        return employeePage.getContent();
    }

    public DatatablePage<Employee> getAllEmployeeDataTable(HttpServletRequest request) {
        // get the data
        int size = Integer.parseInt(request.getParameter("length"));
        int page = Integer.parseInt(request.getParameter("start")) / size;
        System.out.println(request.getParameter("order[0][column]"));
        System.out.println("columns[" + request.getParameter("order[0][column]") + "][data]");
        String sortColName = request.getParameter("columns[" + request.getParameter("order[0][column]") + "][data]");
        String order = request.getParameter("order[0][dir]");
        List<Employee> employee = new ArrayList<>();
        if (order.equals("desc")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).descending());
            employeeRepository.findAll(pageable).forEach(employee::add);
        } else {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).ascending());
            employeeRepository.findAll(pageable).forEach(employee::add);
        }

        long total = employeeRepository.count();
        DatatablePage<Employee> data = new DatatablePage<Employee>();
        data.setData(employee);
        data.setRecordsFiltered((int) total);
        data.setRecordsTotal((int) total);
        data.setDraw(Integer.parseInt(request.getParameter("draw")));
        return data;
    }

    public Employee getEmployeeById(int id) {
        Optional<Employee> optionalEmployee = employeeRepository.findById(id);
        return optionalEmployee.orElse(null);
    }

    public Employee createEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    public Employee updateEmployee(int id, Employee employee) {
        Optional<Employee> optionalEmployee = employeeRepository.findById(id);
        if (optionalEmployee.isPresent()) {
            Employee existingEmployee = optionalEmployee.get();
            existingEmployee.setLastName(employee.getLastName());
            existingEmployee.setFirstName(employee.getFirstName());
            existingEmployee.setEmail(employee.getEmail());
            existingEmployee.setExtension(employee.getExtension());
            existingEmployee.setJobTitle(employee.getJobTitle());
            existingEmployee.setOfficeCode(employee.getOfficeCode());
            return employeeRepository.save(existingEmployee);
        } else {
            return null;
        }
    }

    public void deleteEmployee(int id) {
        employeeRepository.deleteById(id);
    }
}
