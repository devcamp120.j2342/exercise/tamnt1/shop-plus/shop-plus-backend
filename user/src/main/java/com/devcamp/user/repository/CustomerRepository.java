package com.devcamp.user.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;

import com.devcamp.user.models.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

	@Query(value = "SELECT c.country, COUNT(country) as customerPerCountry FROM customers c GROUP BY c.country", nativeQuery = true)
	List<Object[]> findCustomerPerCountry();

}