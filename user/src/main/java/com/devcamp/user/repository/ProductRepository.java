package com.devcamp.user.repository;

import java.math.BigDecimal;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.user.models.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
        Page<Product> findByProductLineId(int productLineId, Pageable pageable);

        @Query(value = "SELECT * FROM products p " +
                        "WHERE (:productVendor IS NULL OR p.product_vendor = :productVendor OR :productVendor = '') " +
                        "AND (:productLineId IS NULL OR p.product_line_id = :productLineId OR :productLineId = 0) " +
                        "AND (:priceFrom IS NULL OR p.buy_price >= :priceFrom) " +
                        "AND (:priceTo IS NULL OR p.buy_price <= :priceTo)", nativeQuery = true)
        Page<Product> filterProduct(
                        @Param("productVendor") String productVendor,
                        @Param("productLineId") Integer productLineId,
                        @Param("priceFrom") BigDecimal priceFrom,
                        @Param("priceTo") BigDecimal priceTo,
                        Pageable pageable);

}
