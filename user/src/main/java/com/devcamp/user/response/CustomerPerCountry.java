package com.devcamp.user.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerPerCountry {
    @JsonProperty("country")
    private String country;
    @JsonProperty("customerPerCountry")
    private int customerPerCountry;

    public CustomerPerCountry() {
    }

    public CustomerPerCountry(String country, int customerPerCountry) {
        this.country = country;
        this.customerPerCountry = customerPerCountry;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getCustomerPerCountry() {
        return customerPerCountry;
    }

    public void setCustomerPerCountry(int customerPerCountry) {
        this.customerPerCountry = customerPerCountry;
    }

}
