package com.devcamp.user.response;

import java.util.List;

public class BaseResponse<T> {
    private long totalCount;
    private List<T> data;

    public BaseResponse() {
    }

    public BaseResponse(long totalCount, List<T> data) {
        this.totalCount = totalCount;
        this.data = data;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

}