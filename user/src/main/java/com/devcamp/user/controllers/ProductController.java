package com.devcamp.user.controllers;

import org.hibernate.service.spi.ServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.user.exportData.ProductExcelExporter;
import com.devcamp.user.models.Product;
import com.devcamp.user.repository.ProductRepository;
import com.devcamp.user.response.BaseResponse;
import com.devcamp.user.services.ProductService;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class ProductController {
    private final ProductService productService;
    private ProductRepository productRepository;

    public ProductController(ProductService productService, ProductRepository productRepository) {
        this.productService = productService;
        this.productRepository = productRepository;
    }

    @GetMapping("/products")
    public ResponseEntity<BaseResponse<Product>> getAllProduct(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size) {
        try {
            BaseResponse<Product> products = productService.getAllProducts(page, size);
            return ResponseEntity.ok(products);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/products/{productLineId}/productLine")
    public ResponseEntity<BaseResponse<Product>> getAllProductByProductLineId(@PathVariable Integer productLineId,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size) {
        try {
            BaseResponse<Product> products = productService.getProductsByProductLineId(page, size, productLineId);
            return ResponseEntity.ok(products);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/products/filter")
    public ResponseEntity<BaseResponse<Product>> getProductByfilter(
            @RequestParam(name = "productLineId", required = false) Integer productLineId,
            @RequestParam(name = "productVendor", required = false) String productVendor,
            @RequestParam(name = "priceFrom", required = false) BigDecimal priceFrom,
            @RequestParam(name = "priceTo", required = false) BigDecimal priceTo,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size) {
        try {
            BaseResponse<Product> products = productService.getProductsByFilter(page, size, productLineId,
                    productVendor, priceFrom, priceTo);
            return ResponseEntity.ok(products);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/products/dataTable")
    public ResponseEntity<Object> getAllProductDataTable(
            HttpServletRequest request) {
        try {
            return new ResponseEntity<>(productService.getAllProductDataTable(request),
                    HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable int id) {

        try {
            Product product = productService.findProductById(id);

            if (product != null) {
                return ResponseEntity.ok(product);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }

    @PostMapping("/products")
    public ResponseEntity<Product> createProduct(@RequestBody Product product) {
        Product createdProduct = productService.createProduct(product);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdProduct);
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<Product> updateProduct(@PathVariable Integer id, @RequestBody Product product) {
        Product updatedProduct = productService.updateProduct(id, product);
        if (updatedProduct != null) {
            return ResponseEntity.ok(updatedProduct);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("products/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable Integer id) {
        productService.deleteProduct(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/export/products/excel")
    public void exportProductsToExcel(HttpServletResponse response) throws IOException {

        List<Product> products = new ArrayList<>();
        productRepository.findAll().forEach(products::add);

        ProductExcelExporter exporter = new ProductExcelExporter();
        exporter.export(products, response);
    }

}
